from flask import render_template, flash, redirect, url_for, Response
from Covid_Tracing_Site import app, db
from Covid_Tracing_Site.forms import CheckInForm, FindBusinessForm, ExportDataForm, BusinessDetailsForm
from Covid_Tracing_Site.models import Business, CheckIn, Customer
from flask_login import current_user, login_required


@app.route('/', methods=['GET', 'POST'])
def home():
    form = FindBusinessForm()
    # Handle POST request and redirect to check in page
    if form.validate_on_submit():
        code = form.code.data
        return redirect(url_for('business', code=code))
    return render_template("home.html", title='Find Business', form=form)


def renderBusiness(b : Business, checkInForm=None, exportForm=None, detailsForm=None):
    # Check user is a manager of the business
    userIsManager = current_user.is_authenticated and \
                    db.session.query(Business).filter(
                        Business.id == b.id,
                        Business.managers.any(id=current_user.id)
                    ).first() is not None

    if checkInForm is None:
        checkInForm = CheckInForm()
    if exportForm is None:
        exportForm = ExportDataForm()
    if detailsForm is None:
        detailsForm = BusinessDetailsForm()
        detailsForm.bName.data = b.name
        detailsForm.address.data = b.address
        detailsForm.postCode.data = b.postCode

    return render_template('business.html',
                           title=b.name,
                           business=b,
                           userIsManager=userIsManager,

                           checkInForm=checkInForm,
                           exportForm=exportForm,
                           detailsForm=detailsForm
                           )


@app.route('/business/<int:code>/', methods=['GET'])
def business(code):
    # Check business with code exists
    b = Business.query.get(code)
    if not b:
        flash(f'Business with code {code} not found', 'danger')
        return redirect(url_for('home'))

    return renderBusiness(b, CheckInForm(), ExportDataForm())


@app.route('/business/<int:code>/check_in', methods=['POST'])
def checkIn(code):
    # Check business with code exists
    b = Business.query.get(code)
    if not b:
        flash(f'Business with code {code} not found', 'danger')
        return redirect(url_for('home'))

    # Handle POST request
    form = CheckInForm()
    if not form.validate_on_submit():
        return renderBusiness(b, checkInForm=form)

    # Create customer
    customer = Customer(
        name=form.name.data,
        email=form.email.data,
        phoneNumber=form.phoneNumber.data
    )

    # Create check in
    checkIn = CheckIn()
    checkIn.customer = customer

    # Add check in to database
    b.checkIns.append(checkIn)
    db.session.commit()

    # Show success alert
    flash(f'Successfully checked in to {b.name}', 'success')
    return redirect(url_for('home'))


@app.route('/business/<int:code>/export', methods=['POST'])
@login_required
def exportData(code):
    # Check business with code exists
    b = Business.query.get(code)
    if not b:
        flash(f'Business with code {code} not found', 'danger')
        return redirect(url_for('home'))

    # Check user is a manager of the business
    res = db.session.query(Business).filter(
        Business.id == b.id,
        Business.managers.any(id=current_user.id)
    ).first()
    if not res:
        flash(f'You are not a manager of {b.name}', 'danger')
        return redirect(url_for('home'))

    form = ExportDataForm()
    if not form.validate_on_submit():
        return renderBusiness(b, exportForm=form)

    startDate = form.startDate.data
    endDate = form.endDate.data

    # Get check in's within given dates
    checkIns = db.session.query(CheckIn).filter(
        CheckIn.business == b,
        CheckIn.date >= startDate,
        CheckIn.date <= endDate
    ).all()

    # Convert check ins to comma separated values
    data = [c.getCsv() for c in checkIns]
    data.insert(0, 'DateTime,Name,Email,PhoneNumber')
    csv = '\n'.join(data)

    fileName = b.name.replace(' ', '-') + '_' + \
               b.postCode.replace(' ', '') + '_' + \
               f'{startDate:%Y-%m-%d}_{endDate:%Y-%m-%d}.csv'
    headers = {
        'Content-disposition': f'attachment; filename={fileName}'
    }

    # Return file for download
    return Response(
        csv,
        mimetype='text/csv',
        headers=headers
    )


@app.route('/business/<int:code>/edit_details', methods=['POST'])
@login_required
def editBusinessDetails(code):
    # Check business with code exists
    b = Business.query.get(code)
    if not b:
        flash(f'Business with code {code} not found', 'danger')
        return redirect(url_for('home'))

    # Check user is a manager of the business
    res = db.session.query(Business).filter(
        Business.id == b.id,
        Business.managers.any(id=current_user.id)
    ).first()
    if not res:
        flash(f'You are not a manager of {b.name}', 'danger')
        return redirect(url_for('home'))

    form = BusinessDetailsForm()
    if not form.validate_on_submit():
        return renderBusiness(b, detailsForm=form)

    b.name = form.bName.data
    b.address = form.address.data
    b.postCode = form.postCode.data
    db.session.commit()

    return redirect(url_for('business', code=code))