from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, IntegerField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from Covid_Tracing_Site.models import User, BusinessOneTimeToken, Business


class RegistrationForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(min=2, max=25)])
    email = StringField('Email', validators=[DataRequired(), Length(min=2, max=120), Email()])

    securityQuestionAnswer = StringField('What is the name of your first employer?',
                                         validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    passwordConfirm = PasswordField('Confirm Password',
                                    validators=[DataRequired(), EqualTo('password')])

    businessToken = StringField('Business Account OTT',
                                validators=[DataRequired(), Length(min=8, max=8)])

    submit = SubmitField('Sign Up')

    # Check email is not already taken
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('Email already registered to an account')

    def validate_businessToken(self, businessToken):
        t = BusinessOneTimeToken.query.filter_by(token=businessToken.data).first()
        if not t:
            raise ValidationError('Business Account OTT not valid')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Length(min=2, max=120), Email()])
    password = PasswordField('Password', validators=[DataRequired()])

    submit = SubmitField('Login')


class CheckInForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(min=2, max=25)])
    email = StringField('Email', validators=[DataRequired(), Length(min=2, max=120), Email()])
    phoneNumber = StringField('Phone Number', validators=[DataRequired(), Length(min=4, max=15)])

    submit = SubmitField('Submit')


class FindBusinessForm(FlaskForm):
    code = StringField('Code', validators=[DataRequired()])
    submit = SubmitField('Find Business')

    def validate_code(self, code):
        business = Business.query.get(code.data)
        if not business:
            raise ValidationError(f'Business with code ({code.data}) could not be found')


class ExportDataForm(FlaskForm):
    startDate = DateField('Start Date', validators=[DataRequired()])
    endDate = DateField('End Date', validators=[DataRequired()])

    submit = SubmitField('Download Data')


class BusinessDetailsForm(FlaskForm):
    bName = StringField('Name', validators=[DataRequired(), Length(min=2, max=50)])
    address = StringField('Address', validators=[DataRequired(), Length(min=2, max=120)])
    postCode = StringField('Post Code', validators=[DataRequired(), Length(min=2, max = 10)])

    submit = SubmitField('Save')