from Covid_Tracing_Site import db, loginManager
from flask_login import UserMixin
from datetime import datetime


@loginManager.user_loader
def load_user(id):
    return User.query.get(int(id))


# Join table
business_managers = db.Table('business_managers',
                             db.Column('user_id', db.Integer, db.ForeignKey('user.id'), nullable=False),
                             db.Column('business_id', db.Integer, db.ForeignKey('business.id'), nullable=False)
)


class Customer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    phoneNumber = db.Column(db.String(15), nullable=False)

    checkIn = db.relationship('CheckIn', backref='customer', lazy=True)


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(25), nullable=False)
    email = db.Column(db.String(120), nullable=False, unique=True)
    passwordHash = db.Column(db.String(60), nullable=False)

    securityAnswerHash = db.Column(db.String(60), nullable=False)

    registered = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    lastLogin = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return f'User("{self.name}", "{self.email}")'


class Business(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)

    address = db.Column(db.String(120), nullable=False)
    postCode = db.Column(db.String(10), nullable=False)

    managers = db.relationship('User', secondary=business_managers,
                               backref=db.backref('businesses', lazy=True))
    checkIns = db.relationship('CheckIn', backref='business', lazy=True)

    tokens = db.relationship('BusinessOneTimeToken', backref='business', lazy=True)

    def __repr__(self):
        return f'Business("{self.name}", "{self.postCode}")'


class CheckIn(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)

    customerId = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=False)
    businessId = db.Column(db.Integer, db.ForeignKey('business.id'), nullable=False)

    def __repr__(self):
        return f'CheckIn("{self.date}", "{self.customeId}")'

    def getCsv(self):
        return f'{self.date:%Y-%m-%d %H:%M:%S},{self.customer.name},{self.customer.email},{self.customer.phoneNumber}'


class BusinessOneTimeToken(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    businessId = db.Column(db.Integer, db.ForeignKey('business.id'), nullable=False)

    token = db.Column(db.String(8), nullable=False, unique=True)