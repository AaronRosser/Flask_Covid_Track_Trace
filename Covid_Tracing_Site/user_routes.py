from flask import render_template, flash, redirect, url_for, request
from Covid_Tracing_Site import app, db, bcrypt
from Covid_Tracing_Site.forms import RegistrationForm, LoginForm
from Covid_Tracing_Site.models import User, BusinessOneTimeToken
from flask_login import login_user, current_user, logout_user
from datetime import datetime


@app.route('/register', methods=['GET', 'POST'])
def register():
    # Check if user logged in
    if current_user.is_authenticated:
        return redirect(url_for('home'))

    form = RegistrationForm()

    # Handle POST request
    if form.validate_on_submit():
        token = BusinessOneTimeToken.query.filter_by(token=form.businessToken.data).first()
        b = token.business

        # Delete token

        # Add user to database
        passwordHash = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(
            name=form.name.data,
            email=form.email.data,
            passwordHash=passwordHash,
            securityAnswerHash=form.securityQuestionAnswer.data
        )

        b.managers.append(user)
        db.session.commit()

        # Show success alert
        flash(f'User {form.name.data} was created successfully. You can now login.', 'success')
        return redirect(url_for('login'))

    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    # Check if user logged in
    if current_user.is_authenticated:
        return redirect(url_for('home'))

    form = LoginForm()

    # Handle POST request
    if form.validate_on_submit():
        # Check for user
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.passwordHash, form.password.data):
            login_user(user)
            # Show success alert
            flash(f'Login successful', 'success')

            # Update last login date time
            user.lastLogin = datetime.now()
            db.session.commit()

            # Redirect if user was trying to access restricted page
            redirectPage = request.args.get('next')
            if redirectPage:
                return redirect(redirectPage)

            return redirect(url_for('home'))
        flash(f'Login Failed. Provided details were incorrect.', 'danger')
    return render_template('login.html', title='Login', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))