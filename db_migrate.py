from Covid_Tracing_Site import app, db
from flask_sqlalchemy import SQLAlchemy
from Covid_Tracing_Site.models import *
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()