from Covid_Tracing_Site import db
from Covid_Tracing_Site.models import *

u = User(
    name='Aaron',
    email='ar@ar.co.uk',
    passwordHash='example',
    securityAnswerHash='qwertyuiop'
)
# db.session.add(u)
# db.session.commit()

b1 = Business(
    name='Leeds Uni',
    address='University of Leeds, Woodhouse, Leeds',
    postCode='LS2 9JT',
)
b1Token = BusinessOneTimeToken(
    businessId=b1.id,
    token='LeedsUni'
)
b1.tokens.append(b1Token)
db.session.add(b1)

b2 = Business(
    name='The Edge',
    address='University of Leeds, Willow Terrace Road, Leeds',
    postCode='LS2 9JT',
)
b2Token = BusinessOneTimeToken(
    businessId=b2.id,
    token='The_Edge'
)
b2.tokens.append(b2Token)
db.session.add(b2)

b3 = Business(
    name='O2 Academy Leeds',
    address='55 Cookridge St, Leeds',
    postCode='LS2 3AW',
)
b3Token = BusinessOneTimeToken(
    businessId=b3.id,
    token='O2_Leeds'
)
b3.tokens.append(b3Token)
db.session.add(b3)

db.session.commit()